import spacy

from elg import FlaskService
from elg.model import Annotation, AnnotationsResponse


class SpacyPL(FlaskService):

    nlp = spacy.load('pl_nask')

    def process_text(self, content):
        doc = self.nlp(content.content)
        tokens = []
        ents = []
        sents = []
        for token in doc:
            head_i = token.head.i
            if head_i == token.i:
                # ROOT
                head_i = -1

            tokens.append(
                Annotation(
                    start=token.idx,
                    end=token.idx+len(token),
                    features={
                        "text": token.text,
                        "lemma": token.lemma_,
                        "upos": token.pos_,
                        "xtag": token.tag_,
                        "deprel": token.dep_,
                        "head": head_i,
                    },
                )
            )

        for ent in doc.ents:
            ents.append(
                Annotation(
                    start=ent.start_char,
                    end=ent.end_char,
                    features={
                        "text": ent.text,
                        "label": ent.label_,
                    },
                )
            )

        for sent in doc.sents:
            sents.append(
                Annotation(
                    start=sent.start_char,
                    end=sent.end_char,
                    features={
                        "text": sent.text,
                    },
                )
            )

        response = AnnotationsResponse(annotations={"tokens": tokens,
                                                    "entities": ents,
                                                    "sentences": sents})
        return response

flask_service = SpacyPL("SpacyPL")
app = flask_service.app
