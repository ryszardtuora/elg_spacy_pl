import spacy
from elg.model import Annotation, AnnotationsResponse

nlp = spacy.load('pl_spacy_model_morfeusz')

doc = nlp("Granice mojego języka oznaczają granice mojego świata")

tokens = []
for token in doc:
    tokens.append(
        Annotation(
            start=token.idx,
            end=token.idx+len(token),
            features={
                "text": token.text,
                "lemma": token.lemma_,
                "pos": token.pos_,
            },
        )
    )

response = AnnotationsResponse(annotations={"tokens": tokens})
print(response)
