# Spacy-PL

Implementation of an ELG compatible service from [https://github.com/ipipan/spacy-pl](https://github.com/ipipan/spacy-pl).

build and run:
```
docker build --no-cache -t elg_spacy:latest .
docker run -p 8000:8000 elg_spacy:latest
```

test:

`curl -X POST -H "Content-Type: application/json" --data '{"type": "text", "content": "Granice mojego języka oznaczają granice mojego świata. Mój świat kończy się na Barcelonie.", "params": {}}' http://localhost:8000/process`
